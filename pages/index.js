import React, { useState } from 'react';
import Pokedex from 'pokedex-promise-v2';
import classNames from 'classnames';

const Index = () => {
  const [query, setQuery] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [results, setResults] = useState(null);

  const P = new Pokedex();

  const closeModal = () => {
    console.log('here');
    setIsActive(false);
  };

  const handleSubmit = async (event) => {
    setIsLoading(true);

    event.preventDefault();

    try {
      const response = await P.getPokemonByName(query.toLowerCase());
      console.log(isActive);
      setResults(
        <React.Fragment>
          <div className="modal-background"></div>
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">{response.name} (#{response.id})</p>
              <button className="delete" onClick={closeModal}></button>
            </header>
            <section className="modal-card-body">
              <div className="card-image">
                <div className="columns">
                  <div className="column is-6">
                    <figure className="image is-4by3">
                      <img src={response.sprites.front_default} />
                    </figure>
                  </div>
                  <div className="column is-6">
                    <figure className="image is-4by3">
                      <img src={response.sprites.back_default} />
                    </figure>
                  </div>
                </div>
              </div>
              <table className="table is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>Property</th>
                    <th>Value</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Name</td>
                    <td>{response.name}</td>
                  </tr>
                  <tr>
                    <td>Number</td>
                    <td>#{response.id}</td>
                  </tr>
                  <tr>
                    <td>Height</td>
                    <td>{response.height}</td>
                  </tr>
                  <tr>
                    <td>Types</td>
                    <td>
                      {
                        response.types.map(
                          (type) => type.type.name
                        ).join('/')
                      }
                    </td>
                  </tr>
                </tbody>
              </table>
              <table className="table is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>Stat</th>
                    <th>Base value</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    response.stats.map(
                      (stat) => {
                        return (
                          <tr>
                            <td>{stat.stat.name}</td>
                            <td>{stat.base_stat}</td>
                          </tr>
                        );
                      }
                    )
                  }
                </tbody>
              </table>
              <table className="table is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>Moves</th>
                    <th>Level learned</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    response.moves
                      .filter(
                        (move) => move.version_group_details[0].level_learned_at !== 0
                      )
                      .map(
                        (move) => {
                          return (
                            <tr>
                              <td>{move.move.name}</td>
                              <td>{move.version_group_details[0].level_learned_at}</td>
                            </tr>
                          );
                        }
                      )
                  }
                </tbody>
              </table>
            </section>
            <footer className="modal-card-foot">
              <button className="button" onClick={closeModal}>Close</button>
            </footer>
          </div>
        </React.Fragment>
      );
    } catch (error) {
      console.log(error);
      setResults(
        <React.Fragment>
          <h1 className="title">
            No results :(
          </h1>
        </React.Fragment>
      );
    } finally {
      setIsActive(true);
      setIsLoading(false);
    }
  };

  return (
    <React.Fragment>
      <section className="hero is-light is-fullheight">
        <div className="hero-body">
          <div className="container">
            <div className="columns">
              <div className="column is-6">
                <h1 className="title">
                  Demo for PeoplePerHour
                </h1>
                <p className="subtitle">
                  Search for Pokémon!
                </p>
                <form onSubmit={handleSubmit}>
                  <label className="label">Enter a Pokémon name</label>
                  <div className="field has-addons">
                    <div className="control is-expanded">
                      <input
                        type="text"
                        value={query}
                        onChange={e => setQuery(e.target.value)}
                        className="input is-fullwidth"
                        placeholder="Examples: Pikachu, Charizard, Mewtwo"
                        autoFocus={true}
                      />
                    </div>
                    <div className="control">
                      <button
                        type="submit"
                        className={classNames('button', 'is-info', { 'is-loading': isLoading })}
                      >
                        Search
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <div className={classNames('modal', { 'is-active': isActive })}>
                {results}
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default Index;
