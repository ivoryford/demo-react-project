import '../node_modules/bulma/bulma.sass';

const App = ({ Component, pageProps }) => {
  return <Component {...pageProps} />
};

export default App;
