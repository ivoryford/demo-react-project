
# demo-react-project

This is a demo project created for PeoplePerHour to demonstrate development skills.

## To run

We recommend this program to be run by an engineer. Non-engineers can verify the attached screenshots.

```
yarn
yarn dev
```
